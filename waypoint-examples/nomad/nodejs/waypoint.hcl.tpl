project = "example-nodejs"

app "example-nodejs" {

  build {
    use "pack" {}
    registry {
        use "docker" {
          image = "DOCKER_HUB_ACCOUNT/waypoint-nodejs-example"
          tag = "1"
        }
    }
 }

  deploy { 
    use "nomad" {
      datacenter = "pondus-dc"
    }
  }

}
